#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Archive Manager for SDS archive.
 ** Needs fairly recent obspy (>=1.0), python3 and docopt.

Usage:
    ivqf-dw.am [-v ...] [options] <archive>

Options:
  -a, --archive <archive> SDS archive to work on
  -v, --verbose           increase verbosity.
  -h, --help              display this help and exit.
  --version               output version information and exit.

"""
# http://www.isc.ac.uk/standards/phases/
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.1.0"
__status__ = "Develop"

import sys, errno
import os.path

import logging

from docopt import docopt
from datetime import datetime

logging.basicConfig(format='# %(filename)s : %(message)s')
log = logging.getLogger()

from fdsntools.download import sdsArchive

if __name__ == "__main__":
#
    try:
        cli = docopt(__doc__, version=__version__)
        #
        # set verbose level
        if cli['--verbose'] >= 2:
            log.setLevel(logging.DEBUG)
        elif cli['--verbose'] == 1:
            log.setLevel(logging.INFO)
        log.debug("cli : %s", str(cli))
        #
        # setup local SDS archive
        log.info("%s : opening local SDS archive", cli["<archive>"])
        archive = sdsArchive(cli["<archive>"])
        #
        archive.getStatus()

        print(archive)

    except KeyboardInterrupt:
#    except:
        log.warning("Program halted")
        sys.exit()
