#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Download waveforms from EIDA node(s) and/or local miniSEED archives.
config files are simple INI files with
All options needed for EIDA data retrieval are contained in simple .INI files
(see -t option to get a template).

 ** Needs fairly recent obspy (>=1.0), python3 and docopt.

Usage:
    ivqf-dw.py [-v ...] [options] <config> ...
    ivqf-dw.py -t

Options:
  -o, --outdir <outdir>   store results to outdir as a SDS archive.
                          [default: ivqf.d]
  -d, --data-dir <datadir>
                          use data-dir as base data repository and create
                          data folder named after the ini file.
                          (still to be implemented)
  -t, --template          output template config file and exit
  -a, --archive <archive> set archive as subfolder of data-dir/outdir where the
                          SDS mSEED data is downloaded.
                          [default: archive]
  -i, --inventory <inventory>
                          set inventory as subfolder of data-dir/outdir where
                          station XML inventory files are downloaded.
                          [default: stations]
  -v, --verbose           increase verbosity.
  -h, --help              display this help and exit.
  --version               output version information and exit.

"""
# http://www.isc.ac.uk/standards/phases/
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.1.0"
__status__ = "Develop"

import sys, errno
import os.path

import logging

from docopt import docopt
from datetime import datetime

logging.basicConfig(format='# %(filename)s : %(message)s')
log = logging.getLogger()

from fdsntools.download import sdsArchive, sdsDownloader

from ivqfutils.config import readConfig, getTemplate

if __name__ == "__main__":
#
    try:
        cli = docopt(__doc__, version=__version__)
        #
        # set verbose level
        if cli['--verbose'] >= 2:
            log.setLevel(logging.DEBUG)
        elif cli['--verbose'] == 1:
            log.setLevel(logging.INFO)
        log.debug("cli : %s", str(cli))
        #
        # output config file template
        log.debug("cli : %s", str(cli))
        if(cli['--template']):
            print(getTemplate())
            sys.exit()
        #
        
        # setup local SDS archive
        log.info("%s : setting up local SDS archive", cli["--outdir"])
        archive = sdsArchive(cli["--outdir"])
        #
        # process download options
        for config in cli["<config>"]:
            log.info("%s : processing", config)

            dw = sdsDownloader(readConfig(config), archive)

            #val = input("Enter your value: "); sys.exit()
            dw.run()


    except KeyboardInterrupt:
#    except:
        log.warning("Program halted")
        sys.exit()
