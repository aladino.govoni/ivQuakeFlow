# -*- coding: utf-8 -*-
"""
"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.0.1"
__status__ = "Develop"

import sys, os
import traceback

import logging
log = logging.getLogger(__name__)

import json
def readjson(inFile):
    log.info("%s : reading options", inFile)
    with open (inFile, "r") as hF:
        data = json.load(hF)

    log.debug("%s : %s", inFile, str(data))
    return data

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

#from obspy import UTCDateTime

def as_dict(config):
    """
    Converts a ConfigParser object into a dictionary.

    The resulting dictionary has sections as keys which point to a dict of the
    sections options as key => value pairs.
    """
    the_dict = {}
    for section in config.sections():
        the_dict[section] = {}
        for key, val in config.items(section):
            the_dict[section][key] = val
    return the_dict

def readini(inFile):
    """
    """
    config = ConfigParser(converters={'list': lambda x: [i.strip() for i in x.split(',')]})

    #    converters={'list': lambda x: [i.strip() for i in x.split(',')]})

    config.read(inFile)
    log.debug("config : %s", vars(config))

    #print(config.get('select', 'channel_priorities'))
    #print(config.getlist('select', 'channel_priorities'))

    # parse some options as list
    chan_p = config.getlist('select', 'channel_priorities')
    loc_p = config.getlist('select', 'location_priorities')

    # convert to dictionary
    cfg = as_dict(config)

    # replace lists
    cfg['select']['channel_priorities'] = chan_p
    cfg['select']['location_priorities'] = loc_p

    # just as debug write json version of cfg dictionary
    #with open ("config.json", "w") as hF:
    #    data = json.dump(cfg, hF, indent=2) #, default=vars)

    log.debug("cfg : %s", str(cfg))
    return cfg

import traceback

def readConfig(inFile):
    """
    """
    try:
        return readjson(inFile)
    except:
        #logging.error(traceback.format_exc())
        pass

    try:
        return readini(inFile)
    except:
        #logging.error(traceback.format_exc())
        pass

    raise ValueError("Format error : not json nor ini")

def getTemplate():
    return __template_config_string__

__template_config_string__ ="""\
[time]
# provide time window in ISO-8601 format
starttime = 2009-04-04T00:00:00
# and either endtime or duration (the latter is not yet implemented)
endtime = 2009-04-11T00:00:00
# duration = 7w  ## possibly same syntax as unix date
#
[Area]
#
# provide a rectangular area specified by latitude and longitude span
#
minlatitude = 41.8
maxlatitude = 42.8
minlongitude = 12.92
maxlongitude = 13.92
#
# or specifying center and ranges
#
#latitude = 42.3
#longitude = 13.42
#lat_width = 1
#lon_width = 1
#
# or provide a circular area specified by center and radius range
#
#latitude = 41.8
#longitude = 13.42
#minradius = 70.0
#maxradius = 90.0
#
# filter stations and channels
[select]
exclude_networks = []
exclude_stations = []
#
# with lists use ':' colon and json like syntax for values (i.e. comma separated)
channel_priorities : HH[ZNE], EH[ZNE], BH[ZNE]
location_priorities : , 00, 10
#
network =
station =
location =
channel =
"""
