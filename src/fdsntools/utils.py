# -*- coding: utf-8 -*-
"""
"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.0.1"
__status__ = "Develop"

import sys, errno
import os.path

def printf(frmt, *args):
    """Dear old C-like printf"""
    sys.stdout.write(frmt % args)

def mkdirs(path):
    """
    Equivalent to mkdir -p.
    returns created path, or None if path already exists.
    """
    try:
        os.makedirs(path)
        return path
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise
        return None

def storeInfo(outfile, lines):
    """write strings to text file (yes, like echo "" > file)"""
    if outfile is not None:
        mkdirs(os.path.dirname(outfile))
        with open(outfile, "w") as hf:
            for line in iter(lines.splitlines()):
                hf.write(line)
            # check that last line has a \n, and append to output
            if line[-1] != '\n':
                hf.write('\n')

def fname():
    """
    :return: name of caller
    """
    return sys._getframe(1).f_code.co_name

def cname():
    """
    :return: name of current class
    """
    return sys._getframe(1).f_code.co_name

def condaGetEnvironmentName():
    return os.environ['CONDA_DEFAULT_ENV'] + " (python-%d.%d)" % (sys.version_info.major, sys.version_info.minor)
#    sys.version_info[0] < 3
