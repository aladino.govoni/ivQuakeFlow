PROJECT := $(notdir $(CURDIR))
MESSAGE := "ivqf-pn : add script for running PhaseNet on archive"
TAG := "tag release"

.PHONY:

all:
	git add --all
	git commit -m $(MESSAGE)
	git push origin master

add:
	git add --all
	git status

help :
	@echo ""
	@echo "** manage gitlab project"
	@echo " make          : add all files, commit and push to origin"
	@echo " make commit   : add all files and commit"
	@echo " make tag      : tag current version"
	@echo " make push     : push to origin"
	@echo " make pull     : pull from origin"
	@echo " make log			: show commit history"
	@echo ""

commit :
	git add --all
	git commit -m $(MESSAGE)

tag:
	git tag -a "v-0.1" -m $(TAG)

push:
	git push origin master

pull:
	git pull

environment:
	# envs can be create from file: conda env create -f environment.yml
	mkdir -p etc/environment
	conda env export | grep -v "^prefix:" > etc/environment/$(PROJECT)-environment-full.yaml
	conda env export  --from-history | grep -v "^prefix:" > etc/environment/$(PROJECT)-environment.yaml

log:
	git log

check:
	@echo "project name : $(PROJECT)"
	@echo "commit message : $(MESSAGE)"
	@echo "tag message : $(TAG)"
