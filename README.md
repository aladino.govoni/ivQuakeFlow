# ivQuakeFlow
Test the original code and port to INGV computing environment.

## Links to original project
* project :				https://github.com/ai4eps/QuakeFlow
* documentation :		https://ai4eps.github.io/QuakeFlow/

## Install
Basically just clone the project
```
git clone https://gitlab.rm.ingv.it/aladino.govoni/ivQuakeFlow.git
```
and add
```
ivQuakeFlow/bin
```
to your PATH. Eventually add to .profile
```
# set PATH so it includes ivqf bin if it exists
if [ -d "$HOME/ivQuakeFlow/bin" ] ; then
    PATH="$HOME/ivQuakeFlow/bin:$PATH"
fi
```

Then create the conda environments needed (see instructions).
```
conda create -f etc/environment/ivqf-dw.yaml
```



To work with jupiter notebooks we clone the base environment and add jupiter (so we can keep base environment clean)
```
conda create --name jbase --clone base
conda activate jbase
conda install jupyter

```
## Project layout

* ivqf-dw : this module downloads the data from EIDA and creates a local SDS archive with all waveforms needed.
* iwqf-pn
* ivqf-ga

## Project management

### Creation from existing draft
First create and eventually cd to local project dir.
Fix and/or setup a suitable .gitignore file.
```
git init
mkdir -p bin etc src var
#
git add .
git commit -m "Project ivQuakeFlow - draft layout"
git remote add origin git@gitlab.rm.ingv.it:aladino.govoni/ivQuakeFlow.git
git push -u origin master
```
Now add a Makefile to automate git.

### Useful templates
  * Python project gitignore template : https://gitlab.rm.ingv.it/aladino.govoni/meglio/-/raw/master/.gitignore
  * Generic project Makefile https://gitlab.rm.ingv.it/aladino.govoni/meglio/-/raw/master/Makefile

### Useful guides/tutorials
  * Markdown guide : https://guides.github.com/features/mastering-markdown/  
  * Basic guide: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html  
  * Workflows: https://www.atlassian.com/git/tutorials/comparing-workflows
