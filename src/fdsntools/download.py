# -*- coding: utf-8 -*-
"""
"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.0.1"
__status__ = "Develop"

import sys, os
import traceback

import logging
log = logging.getLogger(__name__)

from fdsntools.utils import mkdirs

from obspy.clients.filesystem.sds import Client
from obspy import UTCDateTime

class sdsArchive:
    """
    Manage sds local archive using obspy sds client.
    """
    def __init__(self,
        archive_dir = None,     # archive dir
        data_path = "archive",       # data path relative to archive dir
        station_path = "inventory"     # station path relative to archive dir
        ):

        log.debug("%s():", self.__class__.__name__)
        try:
            self.baseDir = archive_dir
            self.data = data_path
            self.stations = station_path

            self.dir = os.path.join(self.baseDir, self.data)
            mkdirs(self.dir)

            self.sds = Client(self.dir)
            log.info("%s : SDS archive setup", self.dir)

            self.info = None

        except:
            logging.error(traceback.format_exc())

    def getStatus(self):
        # get List of (network, station, location, channel) 4-tuples of all
        # available streams in archive.
        nslc=self.sds.get_all_nslc()
        self.info = {}

        # create dictionary with info
        for sta in nslc:
            if sta[0] not in self.info:
                self.info[sta[0]] = {}
            #
            if sta[1] not in self.info[sta[0]]:
                self.info[sta[0]][sta[1]] = {}

            if sta[2] not in self.info[sta[0]][sta[1]]:
                self.info[sta[0]][sta[1]][sta[2]] = {}

            if sta[3][0:2] not in self.info[sta[0]][sta[1]][sta[2]]:
                self.info[sta[0]][sta[1]][sta[2]][sta[3][0:2]] = {}

            if sta[3][2] not in self.info[sta[0]][sta[1]][sta[2]][sta[3][0:2]]:
                self.info[sta[0]][sta[1]][sta[2]][sta[3][0:2]][sta[3][2]] = {}

            self.info[sta[0]][sta[1]][sta[2]][sta[3][0:2]][sta[3][2]]['starttime'] = None
            self.info[sta[0]][sta[1]][sta[2]][sta[3][0:2]][sta[3][2]]['endtime'] = None
            self.info[sta[0]][sta[1]][sta[2]][sta[3][0:2]][sta[3][2]]['availability'] = None
            self.info[sta[0]][sta[1]][sta[2]][sta[3][0:2]][sta[3][2]]['inventory'] = None


        # list of channels
        self.channels = []
        # list of networks
        self.networks = []

        for net in self.info:
            self.networks.append(f'{net}')
            for sta in self.info[net]:
                for loc in self.info[net][sta]:
                    for chan in self.info[net][sta][loc]:
                        self.channels.append(f'{net}.{sta}.{loc}.{chan}')

    def get_baseDir(self):
        return self.baseDir

    def get_dataDir(self):
        return self.data

    def get_stationDir(self):
        return self.stations

    def get_mseed_storage(self,
        network, station, location, channel,
        starttime, endtime):
        # Returning True means that neither the data nor the StationXML file
        # will be downloaded.
        avail = self.sds.get_availability_percentage(network, station, location, channel, starttime, endtime)
        #print(avail)
        #print(avail[0])
        path_str = os.path.join(
            f"{self.dir}",
            f"{starttime.year}",
            f"{network}", f"{station}", f"{channel}.D",
            f"{network}.{station}.{location}.{channel}.D.{starttime.year}.{starttime.julday:03d}"
            )

        if avail[0] < 0.9:
            # If a string is returned the file will be saved in that location.
            log.debug("data path_str set to %s (availability %g, %g gaps/ovlps)", path_str, avail[0], avail[1])
            return path_str
        else:
            log.debug("data path_str set to %s (availability %g, %g gaps/ovlps)", path_str, avail[0], avail[1])
            return True

    def get_stationxml_storage(self):
        path_str = os.path.join(
            self.baseDir,
            self.stations,
            "{network}","{network}.{station}.xml"
            )
        log.debug("station path_str set to %s", path_str)
        #stationxml_storage="/data/ivqf/aquila/stations/{network}/{network}.{station}.xml"
        return path_str

from obspy.clients.fdsn.mass_downloader import \
    CircularDomain, \
    RectangularDomain, \
    Restrictions, \
    MassDownloader
from obspy import UTCDateTime

class sdsDownloader:
    """
    Retrieve waveform data from FDSN datacenters given a selection file.
    Store data locally in a SDS archive togheter with station inventories.
    """
    def __init__(self,
        wave = None,    # wave selector
        sds = None      # sds archive to work on
        ):

        log.debug("%s():", self.__class__.__name__)

        self.sds = sds

        try:
            self.getDomain(wave)
            self.getRestrictions(wave)

            self.mdl = MassDownloader()
        except:
            logging.error(traceback.format_exc())

    def getRestrictions(self, wave):
        # set restrictions
        try:
            #print(wave)
            #print(wave["select"]["channel_priorities"])
            #print(isinstance(list(wave["select"]["channel_priorities"]),list))
            if(isinstance(wave["select"]["channel_priorities"],list)):
                cp = wave["select"]["channel_priorities"]
            else:
                cp = list(wave["select"]["channel_priorities"])
            #
            if(isinstance(wave["select"]["location_priorities"],list)):
                lp = wave["select"]["location_priorities"]
            else:
                lp = list(wave["select"]["location_priorities"])
            #
            print(lp)
            self.restrictions = Restrictions(
                # Defines the temporal bounds of the waveform data.
                starttime = UTCDateTime(wave["time"]["starttime"]),
                endtime =   UTCDateTime(wave["time"]["endtime"]),
                chunklength_in_sec=86400,
                # You might not want to deal with gaps in the data. If this setting is
                # True, any trace with a gap/overlap will be discarded.
                #reject_channels_with_gaps=True,
                reject_channels_with_gaps=False,
                # And you might only want waveforms that have data for at least 95 % of
                # the requested time span. Any trace that is shorter than 95 % of the
                # desired total duration will be discarded.
                #minimum_length=0.95,
                minimum_length=0.0,
                # No two stations should be closer than 10 km to each other. This is
                # useful to for example filter out stations that are part of different
                # networks but at the same physical station. Settings this option to
                # zero or None will disable that filtering.
                minimum_interstation_distance_in_m=0.0,
                #network="BW", station="A*", location="", channel="EH*",
                # Only HH or BH channels. If a station has HH channels, those will be
                # downloaded, otherwise the BH. Nothing will be downloaded if it has
                # neither. You can add more/less patterns if you like.
                #channel_priorities=["HH[ZNE]", "EH[ZNE]", "BH[ZNE]"],
                #channel_priorities=cp,
                channel_priorities=wave["select"]["channel_priorities"],
                # Location codes are arbitrary and there is no rule as to which
                # location is best. Same logic as for the previous setting.
                #location_priorities=["", "00", "10"]
                #location_priorities=lp
                location_priorities=wave["select"]["location_priorities"]
            )
            log.debug("restrictions : %s", vars(self.restrictions))
        except:
            logging.error(traceback.format_exc())

    def getDomain(self, wave):
        # try to guess area
        log.debug("Area : %s", str(wave["Area"]))
        area = wave["Area"]
        # standard rectangular area
        # defined by minlatitude, maxlatitude, minlongitude, maxlongitude
        try:
            self.domain = RectangularDomain(
                minlatitude=area["minlatitude"],    maxlatitude=area["maxlatitude"],
                minlongitude=area["minlongitude"],   maxlongitude=area["maxlongitude"])

            log.debug("domain : [RECT] : %s", vars(self.domain))
            return True

        except:
            logging.error(traceback.format_exc())
            pass

        # rectangular area defined by center and width
        # defined by latitude, longitude, lat_width, lon_width
        try:
            dlat = area["lat_width"]/2;
            dlon = area["lon_width"]/2;
            self.domain = RectangularDomain(
                minlatitude=area["latitude"]-dlat,   maxlatitude=area["latitude"]+dlat,
                minlongitude=area["longitude"]-dlon,   maxlongitude=area["longitude"]+dlon)

            log.debug("domain : [RECT - center] : %s", vars(self.domain))
            return True

        #except Exception as e:
        # except without specifying exception type also catches sys.exit()
        # and KeyboardInterrupt. except Exception is better.
        except:
            logging.error(traceback.format_exc())
            pass

        # circular area defined by center and radius
        # defined by latitude, longitude, lat_width, lon_width
        try:
            self.domain = CircularDomain(
                latitude=area["latitude"],   longitude=area["latitude"],
                minradius=area["minradius"],   maxradius=area["maxradius"])

            log.debug("domain : [CIRCULAR] : %s", vars(self.domain))
            return True

        except:
            logging.error(traceback.format_exc())
            pass

        # raise exception
        raise KeyError("getDomain() : KeyError")

    def run(self):
        """
        """
        self.mdl.download(self.domain, self.restrictions,
            mseed_storage=self.sds.get_mseed_storage,
            stationxml_storage=self.sds.get_stationxml_storage()
            #"/data/ivqf/aquila/stations/{network}/{network}.{station}.xml"
            )
